package properties;

import java.awt.Color;
import java.awt.Dimension;

/**
 * Configuration fonctionnelle et esthetique du simulateur
 **/
public class Constants{
  public final static Dimension screenSize=new Dimension(1280,720);//720p
  
  public final static double mPerPixel=1f;//metre/pixel
  public final static int tickPerS=100;
  
  public final static int fontSize=11;//pixels
  
  public final static String propertiesPath="";
  public final static String scenarioPath="scenario/";
  public final static String profilePath="profile/";
  
  //simulation state
  public static boolean simulation_running=false;
  public static boolean simulation_reset=false;
  public static boolean simulation_show=true;
  
  //voitures
  public static Color car_color_turnSignal=Color.RED;
  public static Color car_color_brake=Color.BLACK;
  public static Color car_color_fieldOfView=Color.BLUE;
  public static boolean car_show_info=false;
  public static boolean car_show_brakeDistance=false;
  public static boolean car_show_viewDistance=false;
  public static int car_info_count=2;//lines of information
  public static int car_info_length=100;//pixels

  //routes
  public final static int road_width=13;//pixels
}
