package properties;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Stack;

import entities.Car;
import entities.Road;

/**
 * Configuration du scenario
 **/

public final class Scenario{

	public static List<Road> roads;
	public static Stack<Car> cars;
	public static boolean generator;
	public static boolean log;
	public static long duration;
	public static ArrayList<String> logParams;
	public static Properties genProps;

	public void loadDefaultProperties(){
		loadProperties(Constants.propertiesPath+Constants.scenarioPath+"default.scenario");
	}

	public void loadProperties(String filename){
		Properties scenarioProp=new Properties();
		Properties profileProp=new Properties();
		InputStreamReader scenario=null;
		InputStreamReader profile=null;
		InputStreamReader gen=null;
		try{
			scenario=new InputStreamReader(Scenario.class.getClassLoader().getResource(filename).openStream());
			scenarioProp.load(scenario);
			//routes
			roads=new ArrayList<Road>();
			//roadi=x1,x2,y1,y2,speed,crossi,priority,torus,carPerS,maxCars,leftLane,rightLane
			int i=0;
			while(scenarioProp.getProperty("road"+i)!=null){
				String[] props=scenarioProp.getProperty("road"+i).split(",");
				List<Road> cross=null;
				if(Integer.parseInt(props[5])>=0&&Integer.parseInt(props[5])<roads.size()&&Integer.parseInt(props[5])!=i){
					cross=new ArrayList<Road>();
					cross.add(roads.get(Integer.parseInt(props[5])));
				}
				Road road=new Road(i,Integer.parseInt(props[0]),Integer.parseInt(props[1]),Integer.parseInt(props[2]),Integer.parseInt(props[3]),Integer.parseInt(props[4]),cross);
				road.priority="true".equals(props[6])||"TRUE".equals(props[6]);
				road.torus="true".equals(props[7])||"TRUE".equals(props[7]);
				road.carPerS=Float.parseFloat(props[8]);
				road.maxCars=Integer.parseInt(props[9]);
				road.leftLane=Integer.parseInt(props[10]);
				road.rightLane=Integer.parseInt(props[11]);
				roads.add(road);
				i+=1;
			}
			//voitures
			cars= new Stack<Car>();
			i=0;
			while(scenarioProp.getProperty("car"+i)!=null){
				String[] carProps = scenarioProp.getProperty("car"+i).split(",");
				profile = new InputStreamReader(Scenario.class.getClassLoader().getResource(Constants.propertiesPath+Constants.profilePath+carProps[0]+".profile").openStream());
				profileProp = new Properties();
				profileProp.load(profile);
				Car newCar = new Car(i,
		                (int)(roads.get(Integer.parseInt(carProps[1])).getMaxSpeed()*Double.parseDouble(profileProp.getProperty("car_speedRatio"))),
		                (double)(Double.parseDouble(profileProp.getProperty("car_speedRatio"))),
		                (int)(Integer.parseInt(profileProp.getProperty("car_acceleration"))),
		                (int)(Integer.parseInt(profileProp.getProperty("car_viewDistance"))),
		                (int)(Integer.parseInt(profileProp.getProperty("car_reactionTime"))),
		                (int)(Integer.parseInt(profileProp.getProperty("car_turnSignalTime"))),
		                (int)(Integer.parseInt(profileProp.getProperty("car_deceleration"))),
		                (int)(Integer.parseInt(profileProp.getProperty("car_length"))),
		                roads.get(Integer.parseInt(carProps[1])));
				newCar.setX(newCar.getX()+(Integer.parseInt(carProps[2])/Constants.mPerPixel)*newCar.getRoad().getX());
				newCar.setY(newCar.getY()+(Integer.parseInt(carProps[2])/Constants.mPerPixel)*newCar.getRoad().getY());
		        cars.add(newCar);
		        i+=1;
			}
			genProps=new Properties();
			gen = new InputStreamReader(Scenario.class.getClassLoader().getResource(Constants.propertiesPath+Constants.profilePath+scenarioProp.getProperty("gen_profile")+".profile").openStream());
			if("true".equals(scenarioProp.getProperty("gen_active"))){
				genProps.load(gen);
				generator = true;
				if(scenarioProp.getProperty("gen_duration")!=null){
					duration = Long.parseLong(scenarioProp.getProperty("gen_duration"));
				}else{
					duration = 0;
				}
			}else{
				generator = false;
			}
			if("true".equals(scenarioProp.getProperty("out_logger"))){
				log = true;
			}else{
				log = false;
			}
			logParams= new ArrayList<String>();
			try{
			for(String param : scenarioProp.getProperty("out_params").split(",")){
				logParams.add(param);
			}
			}catch(Exception e){}
		}catch(IOException ex){
			ex.printStackTrace();
			System.out.println("Scenario demande introuvable");
		}finally{
			if(scenario!=null){
				try{
					scenario.close();
				}catch(IOException e){
					e.printStackTrace();
				}
			}
			if(profile!=null){
				try{
					profile.close();
				}catch(IOException e){
					e.printStackTrace();
				}
			}
			if(gen!=null){
				try{
					gen.close();
				}catch(IOException e){
					e.printStackTrace();
				}
			}
		}

	}

}
