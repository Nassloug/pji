package entities;

import java.awt.Color;
import java.awt.Point;
import java.util.Stack;

import properties.Scenario;
import properties.Constants;
import tools.Trigo;

public class Car{

	private int acceleration,deceleration,viewDistance,id,reactionTime,turnSignalTime,brakeDistance,carLength;
	private double x,y,speed,speedRatio=0;
	private Road road;

	private int defaultSpeed;
	private double lastDistanceFromFrontCar=0;

	private int textShiftY=0;
	public int elapsedReactionTime=0,elapsedTurnLeftSignalTime=0,elapsedTurnRightSignalTime=0;
	public boolean leftTurnSignal=false,rightTurnSignal=false;


	public Car(int id, double speed, double speedRatio, int acceleration, int viewDistance, int reactionTime, int turnSignalTime, int deceleration, int carLength, Road road){
		this.id=id;
		this.speed=speed;
		this.speedRatio=speedRatio;
		defaultSpeed=(int)(road.getMaxSpeed()*speedRatio);
		brakeDistance = getBrakeDistance();
		this.acceleration=acceleration;
		this.deceleration=deceleration;
		this.viewDistance=viewDistance;
		this.reactionTime=reactionTime;
		this.turnSignalTime=turnSignalTime;
		this.road=road;
		this.x=road.getX1();
		this.y=road.getY1();
		this.carLength=carLength;
	}

	public Car(Car c){
		this(c.getId(),c.getSpeed(),c.getSpeedRatio(),c.getAcceleration(),c.getViewDistance(),c.getReactionTime(),c.getTurnSignalTime(),c.getDeceleration(),c.getCarLength(),c.getRoad());
	}

	public void changeRoad(Road road){
		getRoad().carCount--;
		road.carCount++;
		this.road=road;
		defaultSpeed=(int)(road.getMaxSpeed()*speedRatio);
		//translation voiture
		Point b=Trigo.getClosestPointToLineFromPoint(x,y,road.getX1(),road.getY1(),road.getX2(),road.getY2());
		x=b.x;
		y=b.y;
	}

	public boolean isInSight(Car other){
		return (Math.abs(other.getX()-x)<viewDistance && Math.abs(other.getY()-y)<viewDistance)?true:false;
	}

	public boolean isBehind(Car other){
		return distanceFromRoadStart()<other.distanceFromRoadStart();
	}

	public boolean hasPriority(Car other){
		return (other.getRoad().priority)?true:false;
	}

	public boolean collision(Car other){
		return (distanceFromCar(other)<carLength/2)?true:false;
	}

	public double distanceFromCar(Car other){
		if(other==null)return Double.MAX_VALUE;
		lastDistanceFromFrontCar=Trigo.distance2DPointToPoint(getX(),getY(),other.getX(),other.getY());
		return lastDistanceFromFrontCar;
	}

	public double distanceFromRoadStart(){
		return Trigo.distance2DPointToPoint(getX(),getY(),getRoad().getX1(),getRoad().getY1());
	}

	public double distanceFromRoadEnd(){
		return Trigo.distance2DPointToPoint(getX(),getY(),getRoad().getX2(),getRoad().getY2());
	}

	public double distanceFromRoad(Road road){
		return Trigo.distance2DPointToLine(getX(),getY(),road.getX1(),road.getY1(),road.getX2(),road.getY2());
	}

	public boolean update(Stack<Car> closestCars,Car frontCar){
		//System.out.println(getId()+" "+((frontCar==null)?"":frontCar.getId()));
		if(closestCars.size()>0){
			if(frontCar!=null){
				brakeDistance=getBrakeDistance();
				double distanceFromFrontCar=distanceFromCar(frontCar);
				Road leftLane=road.getLeftLane();
				//System.out.println("distance added by clign="+(Scenario.car_turnSignalTime/1000f)*((speed/3.6f)*Constants.mPerPixel));
				//36~25
				if(leftLane!=null){
					if(distanceFromFrontCar<3*carLength+(turnSignalTime/1000f)*((speed/3600f)*Constants.mPerPixel)/2&&frontCar.getRoad().getId()==road.getId()){
						leftTurnSignal=true;
						elapsedTurnLeftSignalTime+=1000/Constants.tickPerS;
						//double par la gauche uniquement si aucun vehicule n'est trop proche
						//et que le temps de clignotant avant depassement est ecoule
						if((distanceFromCar(closestCars.get(0))>carLength+carLength/4||elapsedTurnLeftSignalTime>0)&&elapsedTurnLeftSignalTime>=turnSignalTime||distanceFromFrontCar<1.5*carLength){
							changeRoad(leftLane);
							leftTurnSignal=false;
							elapsedTurnLeftSignalTime=0;
						}
					}else{leftTurnSignal=false;elapsedTurnLeftSignalTime=0;}
				}
				Road rightLane=road.getRightLane();
				//si l'on peux doubler a droite et qu'il n'y a pas de vehicule trop proche
				//superieur a la distance de depassement a gauche sinon le vehicule enchaine les depassement gauche/droite
				if(rightLane!=null){
					if(distanceFromCar(closestCars.get(0))>4*carLength||!rightLane.equals(closestCars.get(0).getRoad())){
						rightTurnSignal=true;
						elapsedTurnRightSignalTime+=1000/Constants.tickPerS;
						//double par la gauche uniquement si aucun vehicule n'est trop proche
						//et que le temps de clignotant avant depassement est ecoule
						if((distanceFromCar(closestCars.get(0))>carLength+carLength/4&&elapsedTurnRightSignalTime>=turnSignalTime)){
							changeRoad(rightLane);
							rightTurnSignal=false;
							elapsedTurnRightSignalTime=0;
						}
					}else{rightTurnSignal=false;elapsedTurnRightSignalTime=0;}
				}

				if(distanceFromFrontCar<=brakeDistance/Constants.mPerPixel&&frontCar.getRoad().getId()==road.getId()&&elapsedTurnLeftSignalTime==0){
					elapsedReactionTime+=1000/Constants.tickPerS;
					if(elapsedReactionTime>=reactionTime){
						speed-=deceleration/(Constants.tickPerS*1f);
						if(speed<0)speed=0;
					}
				}else{elapsedReactionTime=0;}
				//si deux voitures entre en collision
				if(distanceFromFrontCar<carLength/2){
					//System.out.println("La voiture n°"+this.getId()+" entre en collision avec la voiture n°"+frontCar.getId());
					//Window.content.simulationToggler.doClick();//accident pause la simulation
					return true;
				}
				//deplacement vertical du texte informatif de la voiture si les textes se chevauchent
				if(distanceFromFrontCar<Constants.car_info_length){
					setTextShiftY(frontCar.getTextShiftY()+Constants.fontSize*Constants.car_info_count+3);
				}else{
					setTextShiftY(0);
				}
			}
		}else{
			elapsedReactionTime = 0;
		}
		//incremente la position du vehicule en fonction de sa vitesse (km/h) et de l'echelle de la fenetre (metre/pixel) 
		x+=((((speed*1000f)/3600f)/Constants.mPerPixel)/(Constants.tickPerS*1f))*road.getX();
		y+=((((speed*1000f)/3600f)/Constants.mPerPixel)/(Constants.tickPerS*1f))*road.getY();
		if(speed<defaultSpeed&&elapsedReactionTime==0){
			speed+=acceleration/(Constants.tickPerS*1f);
		}else if((speed>defaultSpeed&&elapsedReactionTime==0)){
			speed-=deceleration/(Constants.tickPerS*1f);
		}
		return false;
	}


	//ne doit pas exceder la variable Scenario.car_info_count dans un soucis d'affichage
	public String toString(){
		return "speed :"+(int)speed+"\n"+
				"roadstart@|roadlength:"+(int)distanceFromRoadStart()+"|"+(int)getRoad().getLength();
	}
	//getters&setters
	public int getDeceleration() {
		return deceleration;
	}
	public void setDeceleration(int deceleration) {
		this.deceleration = deceleration;
	}
	public int getReactionTime(){return reactionTime;}
	public void setReactionTime(int reactionTime){this.reactionTime=reactionTime;}
	public int getTurnSignalTime(){return turnSignalTime;}
	public void setTurnSignalTime(int turnSignalTime){this.turnSignalTime=turnSignalTime;}
	public int getId(){return id;}
	public void setId(int id){this.id=id;}
	public double getX(){return x;}
	public void setX(double x){this.x=x;}
	public double getY(){return y;}
	public void setY(double y){this.y=y;}
	public double getSpeed(){return speed;}
	public void setSpeed(int speed){this.speed=speed;}
	public int getAcceleration(){return acceleration;}
	public void setAcceleration(int acceleration){this.acceleration=acceleration;}
	public int getViewDistance(){return viewDistance;}
	public void setViewDistance(int viewDistance){this.viewDistance=viewDistance;}
	public int getCarLength(){return carLength;}
	public void setCarLength(int carLength){this.carLength=carLength;}
	public double getSpeedRatio(){return speedRatio;}
	public void setSpeedRatio(double speedRatio){this.speedRatio = speedRatio;}
	public Color getColor(){
		double deg=getSpeed()/getRoad().getMaxSpeed()/2;
		deg=(deg>1)?1:deg;
		int r=(int)(255*deg);
		int g=(int)(255*(1-deg));
		return new Color(r,g,0);
	}
	public int getTextShiftY(){return textShiftY;}
	public void setTextShiftY(int textShiftY){this.textShiftY=textShiftY;}
	public Road getRoad(){return road;}
	public void setRoad(Road road){this.road=road;}
	public int getBrakeDistance(){return (int)((Math.pow(speed,2)/(2*deceleration))*Constants.mPerPixel);}
	public void setBrakeDistance(int brakeDistance){this.brakeDistance = brakeDistance;}
}
