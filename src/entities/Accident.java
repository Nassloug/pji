package entities;

public class Accident {
  public int x,y;
  public long collisionTick;
  public Car responsible,victim;
  
  public Accident(long collisionTick,int x, int y, Car responsible, Car victim) {
    this.collisionTick=collisionTick;
    this.x=x;
    this.y=y;
    this.responsible=responsible;
    this.victim=victim;
  }
}
