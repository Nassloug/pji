package entities;

import java.util.List;

import properties.Scenario;
import tools.Trigo;

public class Road {

	private double x1,x2,y1,y2,x,y,maxSpeed,length;

  public int id,carCount=0,maxCars=Integer.MAX_VALUE;
  public float carPerS=0;
  public int leftLane,rightLane;
	public List<Road> crossRoads;
	public boolean priority=false,torus=false;
	
	public Road(int id, double x1, double x2, double y1, double y2, double speed, List<Road> crossRoads){
		this.id = id;
		this.x1 = x1;
		this.x2 = x2;
		this.y1 = y1;
		this.y2 = y2;
		length = Trigo.distance2DPointToPoint(x1,y1,x2,y2);
		x = (x2-x1)/length;
		y = (y2-y1)/length;
		maxSpeed = speed;
		this.crossRoads = crossRoads;
	}

	public Road getLeftLane(){return (leftLane<Scenario.roads.size()&&leftLane>=0)?Scenario.roads.get(leftLane):null;}
  public void setLeftLane(int road){leftLane=road;}
	public Road getRightLane(){return (rightLane<Scenario.roads.size()&&rightLane>=0)?Scenario.roads.get(rightLane):null;}
	public void setRightLane(int road){rightLane=road;}

  public double getLength(){return Trigo.distance2DPointToPoint(x1,y1,x2,y2);}
	public double getX(){return x;}
	public void setX(double x){this.x=x;}
	public double getY(){return y;}
	public void setY(double y){this.y=y;}
	public double getX1(){return x1;}
	public void setX1(double x){x1=x;}
	public double getY1(){return y1;}
	public void setY1(double y){y1=y;}
	public double getX2(){return x2;}
	public void setX2(double x){x2=x;}
	public double getY2(){return y2;}
	public void setY2(double y){y2=y;}
	public double getMaxSpeed(){return maxSpeed;}
	public void setMaxSpeed(double speed){maxSpeed=speed;}
	public int getId(){return id;}
	public void setId(int id){this.id=id;}
}
