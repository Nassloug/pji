package display;

import java.awt.Rectangle;

import javax.swing.JFrame;

import properties.Constants;

public class Window extends JFrame{
  
	private static final long serialVersionUID = 1L;
	
	public static WindowContent content;
	
	public Window(String title){
		super(title);
		this.setBounds(new Rectangle(128,128,Constants.screenSize.width,Constants.screenSize.height));
		this.setPreferredSize(getSize());
		this.setMinimumSize(getSize());
		this.setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    Window.content=new WindowContent();
    this.add(content);
    this.setVisible(true);
	}
	
	public void refreshContent(){
	  content.repaint();
	  getToolkit().sync();
	}
}
