package display;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Arc2D;
import java.util.Stack;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import properties.Scenario;
import properties.Constants;
import entities.Accident;
import entities.Car;
import entities.Road;
import main.Controller;

public class WindowContent extends JPanel{

  private static final long serialVersionUID = 1L;
  
  public final JButton simulationToggler;
  final JButton simulationResetter;
	
  public WindowContent(){
    simulationToggler=new JButton((Constants.simulation_running)?"||":">");
    simulationToggler.setToolTipText((Constants.simulation_running)?"Arreter la simulation":"Demarrer la simulation");
    simulationToggler.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e){
        Constants.simulation_running=!Constants.simulation_running;
        simulationToggler.setText((Constants.simulation_running)?"||":">");
        simulationToggler.setToolTipText((Constants.simulation_running)?"Arreter la simulation":"Reprendre la simulation");
      } 
    });
    add(simulationToggler);
    
    simulationResetter=new JButton("[]");
    simulationResetter.setToolTipText("Redemarrer la simulation");
    simulationResetter.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e){
        boolean stopped=false;
        if(Constants.simulation_running){
          simulationToggler.doClick();
          stopped=true;
        }
        int dialogResult = JOptionPane.showConfirmDialog(simulationResetter, "Redemarrer la simulation?");
        if(dialogResult == JOptionPane.YES_OPTION){
          Constants.simulation_reset=true;
        }else{
          if(stopped)simulationToggler.doClick();
        }
      } 
    });
    add(simulationResetter);
    
    final JCheckBox showBrakeDistance=new JCheckBox();
    showBrakeDistance.setToolTipText((Constants.car_show_brakeDistance)?"Cacher la distance de freinage":"Afficher la distance de freinage");
    showBrakeDistance.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e){
        Constants.car_show_brakeDistance=!Constants.car_show_brakeDistance;
        repaint();
        showBrakeDistance.setToolTipText((Constants.car_show_brakeDistance)?"Cacher la distance de freinage":"Afficher la distance de freinage");
      } 
    });
    add(showBrakeDistance);
    final JCheckBox showViewDistance=new JCheckBox();
    showViewDistance.setToolTipText((Constants.car_show_brakeDistance)?"Cacher la distance de vue":"Afficher la distance de vue");
    showViewDistance.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e){
        Constants.car_show_viewDistance=!Constants.car_show_viewDistance;
        repaint();
        showViewDistance.setToolTipText((Constants.car_show_brakeDistance)?"Cacher la distance de vue":"Afficher la distance de vue");
      } 
    });
    add(showViewDistance);
  }
  
	public void paintComponent(Graphics g){
	  Graphics2D g2d =(Graphics2D) g;
	  g2d.clearRect(0, 0, getWidth(), getHeight());
	  paintRoads(g2d);
	  
	  paintCars(g2d);
	  paintAccidents(g2d);
	  paintSimulationInfo(g2d);
	}

  public void paintRoads(Graphics2D g2d){
    g2d.setColor(new Color(0.5f,0.5f,0.5f,0.7f));
    BasicStroke tmp=(BasicStroke) g2d.getStroke();
    g2d.setStroke(new BasicStroke(3f));
    for(Road r:Scenario.roads)g2d.drawLine((int)r.getX1(),(int)r.getY1(),(int)r.getX2(),(int)r.getY2());
    g2d.setStroke(tmp);
  }
  
  public void paintCars(Graphics2D g2d){
    Stack<Car> cpCars=new Stack<Car>();
    cpCars.addAll(Controller.cars);
    for(Car c:cpCars){
      Color carViewDistanceColor=Constants.car_color_fieldOfView;
      int x=(int)c.getX(),y=(int)c.getY();
      //car field of view---------------
      if(Constants.car_show_viewDistance){
        int viewDistance=c.getViewDistance();
        g2d.setColor(new Color(carViewDistanceColor.getRed()/255f,
                               carViewDistanceColor.getGreen()/255f,
                               carViewDistanceColor.getBlue()/255f,
                               0.1f));
        /**g2d.drawOval((int)(x-viewDistance/Constants.mPerPixel),
                     (int)(y-viewDistance/Constants.mPerPixel),
                     (int)((viewDistance*2)/Constants.mPerPixel),
                     (int)((viewDistance*2)/Constants.mPerPixel));
            **/
          g2d.fill(new Arc2D.Double((int)(x-viewDistance/Constants.mPerPixel),
                           (int)(y-viewDistance/Constants.mPerPixel),
                           (int)((viewDistance*2)/Constants.mPerPixel),
                           (int)((viewDistance*2)/Constants.mPerPixel),
                           0,//-Constants.car_fieldOfView/2,
                           360,//Constants.car_fieldOfView,
                           Arc2D.PIE)
        );
      }
      //--------------------------------
      //car brake distance--------------
      if(Constants.car_show_brakeDistance){
        g2d.setColor(Constants.car_color_brake);
        g2d.drawRect((int)(x-(c.getCarLength()/Constants.mPerPixel)/2),
                     (int)(y-(c.getCarLength()/Constants.mPerPixel)/2),
                     (int)((c.getBrakeDistance())/Constants.mPerPixel),
                     c.getCarLength());
      }
      //--------------------------------
      //car body------------------------
      g2d.setColor(c.getColor());
      g2d.fillPolygon(
        new int[]{
          x-c.getCarLength()/2,
          x-c.getCarLength()/2,
          x+c.getCarLength()/2
        },
        new int[]{
        	y+c.getCarLength()/2,
        	y-c.getCarLength()/2,
        	y
        },
        3
      );
      //clignotants
      //gauche
      if(c.leftTurnSignal){
        g2d.setColor(Constants.car_color_turnSignal);
        g2d.fillPolygon(
          new int[]{
            x-c.getCarLength()/2,
            x-c.getCarLength()/2,
            x
          },
          new int[]{
            y-c.getCarLength()/2,
            y,
            y-c.getCarLength()/4
          },
          3
        );
      }
      //droite
      if(c.rightTurnSignal){
        g2d.setColor(Constants.car_color_turnSignal);
        g2d.fillPolygon(
          new int[]{
            x-c.getCarLength()/2,
            x-c.getCarLength()/2,
            x
          },
          new int[]{
            y+c.getCarLength()/2,
            y,
            y+c.getCarLength()/4
          },
          3
        );
      }
      g2d.setColor(Color.BLACK);
      g2d.drawPolygon(
        new int[]{
                (int)(x-c.getCarLength()/2),
                (int)(x-c.getCarLength()/2),
                (int)(x+c.getCarLength()/2)
            },
            new int[]{
            	(int)(y+c.getCarLength()/2),
            	(int)(y-c.getCarLength()/2),
            	(int)(y)
            },
        3
      );
      g2d.drawString(""+(c.elapsedTurnLeftSignalTime+c.elapsedTurnRightSignalTime), x-Constants.fontSize, y-Constants.fontSize);
      //--------------------------------
      //car info------------------------
      //cadre de l'info
      if(Constants.car_show_info){
        Font tmp2=g2d.getFont();
        g2d.setColor(Color.BLACK);
        g2d.drawLine((int) (Math.floor(x)-c.getCarLength()/2), y,
                     (int) (Math.floor(x)-c.getCarLength()/2), y+c.getCarLength()/2+c.getTextShiftY()+Constants.car_info_count*Constants.fontSize);
        g2d.drawLine((int) (Math.floor(x)-c.getCarLength()/2), y+c.getCarLength()/2+c.getTextShiftY()+Constants.car_info_count*Constants.fontSize,
                     (int) (Math.floor(x)-c.getCarLength()/2)+Constants.car_info_length, y+c.getCarLength()/2+c.getTextShiftY()+Constants.car_info_count*Constants.fontSize);
  
        g2d.setFont(new Font("Bell MT",Font.PLAIN,Constants.fontSize));
        int tmp=c.getTextShiftY();
        for (String line : c.toString().split("\n")){
          g2d.drawString(line, x, y+c.getCarLength()+tmp);
          tmp+=Constants.fontSize;
        }
        g2d.setFont(tmp2);
      }
      //--------------------------------
    }
  }
  
  public void paintAccidents(Graphics2D g2d){
    g2d.setColor(Color.RED);
    for(Accident a:Controller.accidents){
      int x=a.x,y=a.y,size=a.victim.getCarLength()/2;
      g2d.drawLine(x-size/2, y-size/2, x+size/2, y+size/2);
      g2d.drawLine(x-size/2, y+size/2, x+size/2, y-size/2);
    }
  }
	
  public void paintSimulationInfo(Graphics2D g2d){
    g2d.setColor(Color.BLACK);
    g2d.drawString("tick N°:"+Controller.ticks, 0, getHeight()-5*Constants.fontSize);
    if(Constants.simulation_running){
      g2d.drawString("simulation speed (ms/tick):"+(System.currentTimeMillis()-Controller.lastTick), 0, getHeight()-4*Constants.fontSize);
      Controller.lastTick=System.currentTimeMillis();
    }else{
      g2d.drawString("simulation speed (ms/tick):"+0, 0, getHeight()-4*Constants.fontSize);
    }
    g2d.drawString("simulation speed aim:"+1000/Constants.tickPerS, 0, getHeight()-3*Constants.fontSize);
    
  }
}
