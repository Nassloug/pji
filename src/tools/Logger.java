package tools;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import main.Controller;

public class Logger{

  private static FileWriter writer;
  private static final char DEFAULT_SEPARATOR=',';

  public static void writeLine(Writer w,List<String> values)throws IOException{
    writeLine(w,values,DEFAULT_SEPARATOR,' ');
  }
  public static void writeLine(Writer w,List<String> values,char separators)throws IOException{
    writeLine(w,values,separators,' ');
  }
  private static String followCVSformat(String value){
    String result=value;
    if(result.contains("\"")){
      result=result.replace("\"","\"\"");
    }
    return result;
  }

  public static void writeLine(Writer w,List<String> values,char separators,char customQuote)throws IOException{
    boolean first=true;
    if(separators==' '){
      separators=DEFAULT_SEPARATOR;
    }
    String s="";
    for(String value:values){
      if(!first){
        s+=separators;
      }
      if(customQuote==' '){
        s+=followCVSformat(value);
      } else{
        s+=customQuote+followCVSformat(value)+customQuote;
      }
      first=false;
    }
    s+="\n";
    w.append(s.toString());
  }

  public static void open(List<String> title) throws IOException{
    int i=0;
    String csvFile="log/log_"+Controller.title+i+".csv";
    while(new File(csvFile).isFile()){
      i++;
      csvFile="log/log_"+Controller.title+i+".csv";
    }
    writer=new FileWriter(csvFile);

    //entete
    writeLine(writer,title);
  }
  public static void log(List<String> line) throws IOException{
    writeLine(writer,line);
  }
  public static void close() throws IOException{
    writer.flush();
    writer.close();
  }
}
