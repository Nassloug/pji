package tools;

import java.awt.Point;

public class Trigo {
  public static double distance2DPointToPoint(double x1,double y1,double x2,double y2){
    return Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
  }
  
  public static double distance2DPointToLine(double x,double y,double x1,double y1,double x2,double y2){
    double AB=distance2DPointToPoint(x,y,x1,y1);
    double BC=distance2DPointToPoint(x1,y1,x2,y2);
    double AC=distance2DPointToPoint(x,y,x2,y2);
    double s=(AB+BC+AC)/2;
    return (2*(double)Math.sqrt(s*(s-AB)*(s-BC)*(s-AC)))/BC;
  }
  
  public static Point getClosestPointToLineFromPoint(double x,double y,double x1,double y1,double x2,double y2){
    double xDelta=x2-x1;
    double yDelta=y2-y1;
    double u=((x-x1)*xDelta+(y-y1)*yDelta)/(xDelta*xDelta+yDelta*yDelta);
    final Point closestPoint;
    if(u<0){
      closestPoint=new Point((int)x1,(int)y1);
    }else if(u>1){
      closestPoint=new Point((int)x2,(int)y2);
    }else{
      closestPoint=new Point((int)Math.round(x1+u*xDelta),(int)Math.round(y1+u*yDelta));
    }
    return closestPoint;
  }
}
