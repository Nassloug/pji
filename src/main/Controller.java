package main;

import java.awt.AWTException;
import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;

import tools.Graph;
import tools.Logger;
import tools.Trigo;
import properties.Scenario;
import properties.Constants;
import display.Window;
import entities.Accident;
import entities.Car;
import entities.Road;

public class Controller {

	public static long ticks=0;
	public static long lastTick=0;

	public static String title="...";
	public Window window;
	//public Graph graph=new Graph(window);

	public static Stack<Car>cars=new Stack<Car>();

	public static List<Accident> accidents=new ArrayList<Accident>();

	public Controller(String windowName,boolean show){
	  if(show)
	    window=new Window(windowName);
  		title=windowName;
	}

	public void update(){
		Stack<Car> tmpCars=new Stack<Car>();
		for(Car c:cars){
			tmpCars.add(c);
		}
		//sort by distance from road start
		Collections.sort(tmpCars, new Comparator<Car>(){
			public int compare(Car c1, Car c2){
				return (int)(c1.distanceFromRoadStart()-c2.distanceFromRoadStart());
			}
		});
		for(int i=0;i<tmpCars.size();i++){
			final Car actualCar=tmpCars.get(i);
			Car frontCar=null;
			Stack<Car> closestCars=new Stack<Car>();
			closestCars.addAll(tmpCars);
			//la voiture actuelle ne compte pas dans les voitures les plus proches
			closestCars.remove(actualCar);
			//remove cars outside actualCar vision
			Iterator<Car> iter=closestCars.iterator();
			while(iter.hasNext()){
				Car c=iter.next();
				if(actualCar.distanceFromCar(c)>actualCar.getViewDistance()){
					iter.remove();
				}else{
					if(frontCar==null){
						if(actualCar.isBehind(c))frontCar=c;
					}else if(actualCar.isBehind(c)&&frontCar.distanceFromCar(actualCar)>c.distanceFromCar(actualCar)){
						frontCar=c;
					}
				}
			}
			//sort by distance from actualCar
			Collections.sort(closestCars, new Comparator<Car>(){
				public int compare(Car c1, Car c2){
					return (int)(c1.distanceFromCar(actualCar)-c2.distanceFromCar(actualCar));
				}
			});
			boolean codeRetour=actualCar.update(closestCars,frontCar);
			//accident
			if(codeRetour){
				try{
					//Logger.log(Arrays.asList(""+ticks,""+actualCar.getId(),""+actualCar.getRoad().getId(),""+actualCar.getX(),""+actualCar.getY(),""+actualCar.getSpeed(),""+accidents.size()));
					if(Scenario.log){
						Logger.log(Arrays.asList(
								""+ticks,
								""+actualCar.getRoad().getId(),
								""+actualCar.getId(),
								""+(int)actualCar.getX(),
								""+(int)actualCar.getY(),
								""+(int)actualCar.getSpeed(),
								""+actualCar.getReactionTime(),
								""+actualCar.getSpeedRatio(),
								""+actualCar.getViewDistance(),
								""+actualCar.getDeceleration(),
								""+actualCar.getAcceleration(),
								""+frontCar.getId(),
								""+(int)frontCar.getX(),
								""+(int)frontCar.getY(),
								""+(int)frontCar.getSpeed(),
								""+frontCar.getReactionTime(),
								""+frontCar.getSpeedRatio(),
								""+frontCar.getViewDistance(),
								""+frontCar.getDeceleration(),
								""+frontCar.getAcceleration(),
								""+accidents.size()));
					}else{
						System.out.print(ticks+",");
						if(Scenario.logParams.contains("roadID"))System.out.print(actualCar.getRoad().getId()+",");
						if(Scenario.logParams.contains("car1ID"))System.out.print(actualCar.getId()+",");
						if(Scenario.logParams.contains("car1X"))System.out.print((int)actualCar.getX()+",");
						if(Scenario.logParams.contains("car1Y"))System.out.print((int)actualCar.getY()+",");
						if(Scenario.logParams.contains("car1Speed"))System.out.print((int)actualCar.getSpeed()+",");
						if(Scenario.logParams.contains("car1ReactionTime"))System.out.print(actualCar.getReactionTime()+",");
						if(Scenario.logParams.contains("car1SpeedRatio"))System.out.print(actualCar.getSpeedRatio()+",");
						if(Scenario.logParams.contains("car1ViewDistance"))System.out.print(actualCar.getViewDistance()+",");
						if(Scenario.logParams.contains("car1Deceleration"))System.out.print(actualCar.getDeceleration()+",");
						if(Scenario.logParams.contains("car1Acceleration"))System.out.print(actualCar.getAcceleration()+",");
						if(Scenario.logParams.contains("car2ID"))System.out.print(frontCar.getId()+",");
						if(Scenario.logParams.contains("car2X"))System.out.print((int)frontCar.getX()+",");
						if(Scenario.logParams.contains("car2Y"))System.out.print((int)frontCar.getY()+",");
						if(Scenario.logParams.contains("car2Speed"))System.out.print((int)frontCar.getSpeed()+",");
						if(Scenario.logParams.contains("car2ReactionTime"))System.out.print(frontCar.getReactionTime()+",");
						if(Scenario.logParams.contains("car2SpeedRatio"))System.out.print(frontCar.getSpeedRatio()+",");
						if(Scenario.logParams.contains("car2ViewDistance"))System.out.print(frontCar.getViewDistance()+",");
						if(Scenario.logParams.contains("car2Deceleration"))System.out.print(frontCar.getDeceleration()+",");
						if(Scenario.logParams.contains("car2Acceleration"))System.out.print(frontCar.getAcceleration()+",");
						System.out.print(accidents.size()+"\n");
					}
				}catch(IOException e){e.printStackTrace();}
				cars.remove(actualCar);
				cars.remove(frontCar);
				accidents.add(new Accident(ticks,(int)actualCar.getX(),(int)actualCar.getY(),actualCar,frontCar));
			}
			//changement de route
			if(actualCar.getX()>=actualCar.getRoad().getX2()&&actualCar.getY()<=actualCar.getRoad().getY2()){
			  /**
			  if(actualCar.getRoad().crossRoads.get(0)!=null){
			    if(actualCar.distanceFromRoadEnd()<5*actualCar.getCarLength()){
			      boolean freine=false;
			      for(Car c:closestCars){
			        if(c.getRoad()==actualCar.getRoad().crossRoads.get(0)&&Trigo.distance2DPointToPoint(actualCar.getX(),actualCar.getY(),actualCar.getRoad().crossRoads.get(0).getX2(),actualCar.getRoad().crossRoads.get(0).getY2())<8*actualCar.getCarLength()){
			          freine=true;break;
			        }
			      }
			      if(freine)actualCar.setSpeed((int)(actualCar.getSpeed()-actualCar.getDeceleration()/(Constants.tickPerS*1f)));
			    }
			  }**/
				if(actualCar.getRoad().crossRoads!=null){
					actualCar.changeRoad(actualCar.getRoad().crossRoads.get(0));
				}
			}
			//fin de route = destruction vehicule
			if(actualCar.distanceFromRoadStart()>actualCar.getRoad().getLength()){
				if(actualCar.getRoad().torus){
					actualCar.setX(actualCar.getRoad().getX1());
					actualCar.setY(actualCar.getRoad().getY1());
				}else{
					cars.remove(actualCar);
					actualCar.getRoad().carCount--;
				}
			}
		}
		if(Constants.simulation_show)window.refreshContent();
	}

	public List<Point> getAccidentReport(){
		List<Point> report=new ArrayList<Point>();
		report.add(new Point(0,0));
		for(int i=0;i<accidents.size();i++){
			report.add(new Point((int)(accidents.get(i).collisionTick*(1000/Constants.tickPerS)/1000),i+1));
		}
		return report;
	}

	public void genCar(){
		for(Road r:Scenario.roads){
			if(ticks>0&&
					ticks%((int)(Constants.tickPerS*(1/r.carPerS)))==0&&
					(r.carCount<r.maxCars||r.maxCars==0)){
				cars.add(new Car((cars.isEmpty())?1:cars.lastElement().getId()+1,
						(int)(r.getMaxSpeed()*Double.parseDouble(Scenario.genProps.getProperty("car_speedRatio"))),
						(double)(Double.parseDouble(Scenario.genProps.getProperty("car_speedRatio"))),
						(int)(Integer.parseInt(Scenario.genProps.getProperty("car_acceleration"))),
						(int)(Integer.parseInt(Scenario.genProps.getProperty("car_viewDistance"))),
						(int)(Integer.parseInt(Scenario.genProps.getProperty("car_reactionTime"))),
						(int)(Integer.parseInt(Scenario.genProps.getProperty("car_turnSignalTime"))),
						(int)(Integer.parseInt(Scenario.genProps.getProperty("car_deceleration"))),
						(int)(Integer.parseInt(Scenario.genProps.getProperty("car_length"))),
						r));
				r.carCount++;
			}
		}
	}

	//cycle de simulation, un cycle = un tick
	//renvoit false si le temps du scénario est écoulé
	public boolean cycle(){
	  if(Constants.simulation_running){
      if(Scenario.generator){
        genCar();
      }
      update();
      //c.graph.setScores(c.getAccidentReport());
      //c.graph.repaint();
      ticks+=1;
      if(Scenario.duration>0&&Scenario.duration<=ticks){
        return false;
      }
    }
    if(Constants.simulation_reset){
      cars.clear();
    }
    return true;
	}
	
	// args[0]=scenario (string)
	// args[1]=mode console (boolean)
	public static void main(String[] args) throws AWTException, IOException{
		String scenarioPath=(args[0]!=null)?args[0]:"defaultgenerator";
		//String scenario="default";
		//String scenario="view";
		//String scenario="speed";
		Scenario scenario=new Scenario();
		scenario.loadProperties(Constants.propertiesPath+Constants.scenarioPath+scenarioPath+".scenario");
		cars.addAll(Scenario.cars);
		Constants.simulation_show=("true".equals(args[1]));
		final Controller c=new Controller(scenarioPath,Constants.simulation_show);
		//Logger.open(Arrays.asList("Tick","CarID","RoadID","CarX","CarY","CarSpeed","nAccidents"));
		if(Scenario.log){
		  Logger.open(Arrays.asList(
				"tick",
				"roadID",
				"car1ID",
				"car1X",
				"car1Y",
				"car1Speed",
				"car1ReactionTime",
				"car1SpeedRatio",
				"car1ViewDistance",
				"car1Deceleration",
				"car1Acceleration",
				"car2ID",
				"car2X",
				"car2Y",
				"car2Speed",
				"car2ReactionTime",
				"car2SpeedRatio",
				"car2ViewDistance",
				"car2Deceleration",
				"car2Acceleration",
				"nAccidents"));
		}else{
		  System.out.print("tick,");
      if(Scenario.logParams.contains("roadID"))System.out.print("roadID,");
      if(Scenario.logParams.contains("car1ID"))System.out.print("car1ID,");
      if(Scenario.logParams.contains("car1X"))System.out.print("car1X,");
      if(Scenario.logParams.contains("car1Y"))System.out.print("car1Y,");
      if(Scenario.logParams.contains("car1Speed"))System.out.print("car1Speed,");
      if(Scenario.logParams.contains("car1ReactionTime"))System.out.print("car1ReactionTime,");
      if(Scenario.logParams.contains("car1SpeedRatio"))System.out.print("car1SpeedRatio,");
      if(Scenario.logParams.contains("car1ViewDistance"))System.out.print("car1ViewDistance,");
      if(Scenario.logParams.contains("car1Deceleration"))System.out.print("car1Deceleration,");
      if(Scenario.logParams.contains("car1Acceleration"))System.out.print("car1Acceleration,");
      if(Scenario.logParams.contains("car2ID"))System.out.print("car2ID,");
      if(Scenario.logParams.contains("car2X"))System.out.print("car2X,");
      if(Scenario.logParams.contains("car2Y"))System.out.print("car2Y,");
      if(Scenario.logParams.contains("car2Speed"))System.out.print("car2Speed,");
      if(Scenario.logParams.contains("car2ReactionTime"))System.out.print("car2ReactionTime,");
      if(Scenario.logParams.contains("car2SpeedRatio"))System.out.print("car2SpeedRatio,");
      if(Scenario.logParams.contains("car2ViewDistance"))System.out.print("car2ViewDistance,");
      if(Scenario.logParams.contains("car2Deceleration"))System.out.print("car2Deceleration,");
      if(Scenario.logParams.contains("car2Acceleration"))System.out.print("car2Acceleration,");
      System.out.print("nAccidents\n");
		}
		if(Constants.simulation_show){
  		new Timer().schedule(
  			new TimerTask(){
  				public void run(){
  					boolean cycleDone=c.cycle();
  					if(!cycleDone)this.cancel();
  				}
  			},
  			0,//délai avant le premier appel
  			1000/Constants.tickPerS//délai entre deux appels consécutifs
  			);
		}else{
		  Constants.simulation_running=true;
		  while(true){
        boolean cycleDone=c.cycle();
        if(!cycleDone)break;
		  }
		}
	}
}
