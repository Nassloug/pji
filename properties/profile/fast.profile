#ratio vitesse
car_speedRatio=1.2

#acceleration en km/h/s (11 en moyenne)
car_acceleration=11

#deceleration en km/h/s (40 par temps sec 30 par temps humide cf wikipedia)
car_deceleration=40

#distance de vue en metres ("une acuite de 10/10 permet de lire un panneau a 150m")
car_viewDistance=150

#taille de la voiture en pixels
car_length=13

#temps de reaction de la voiture en ms
car_reactionTime=1000

#angle de vue de la voiture en °
car_fieldOfView=124

#anticipation clignotant
car_turnSignalTime=3000